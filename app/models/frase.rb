class Frase < ApplicationRecord

  belongs_to :creador, class_name: 'Persona', foreign_key: :creador_id

  validates :texto, presence: true
  validates :fecha, in_past: true

end
