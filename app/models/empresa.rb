class Empresa < ApplicationRecord
  validates :nombre, :url, presence: true

  def to_s(format = true)
    %(#{nombre})
  end

end
