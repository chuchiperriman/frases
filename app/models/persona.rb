class Persona < ApplicationRecord
  belongs_to :empresa

  def to_s
    "#{nombre} #{apellidos}"
  end
end
