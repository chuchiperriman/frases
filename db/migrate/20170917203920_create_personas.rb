class CreatePersonas < ActiveRecord::Migration[5.1]
  def change
    create_table :personas do |t|
      t.string :nombre
      t.string :apellidos
      t.references :empresa, foreign_key: true

      t.timestamps
    end
  end
end
