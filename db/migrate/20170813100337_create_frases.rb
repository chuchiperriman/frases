class CreateFrases < ActiveRecord::Migration[5.1]
  def change
    create_table :frases do |t|
      t.text :texto
      t.datetime :fecha

      t.timestamps
    end
  end
end
