class AddCreadorToFrase < ActiveRecord::Migration[5.1]
  def up
		add_column :frases, :creador_id, :integer
		add_foreign_key :frases, :personas, column: :creador_id, name: 'frases_creador_id_fkey'
  end

  def down
  	remove_column :frases, :creador_id, :integer
  end
end
