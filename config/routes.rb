Rails.application.routes.draw do
  resources :personas
  resources :empresas
  resources :frases
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: redirect('/frases')
end
